# Docker image with Python installed by pyenv, Ansible and Galaxy modules installed

This image has a recent Python version installed using pyenv, then uses pip to install Ansible and Galaxy modules.

Have purposely not pinned exact versions of Ansible or requirements.txt so that next image build gets newer set of dependencies.  But copied [freeze.txt](freeze.txt), as historical record of final exact pip modules.

### Pulling image

```
registry.gitlab.com/gitlab-pipeline-helpers/docker-python-pyenv-ansible-user-venv:latest
```

### Creating tag that invokes Gitlab Pipeline

```
newtag=v1.0.0
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip && git tag $newtag && git push origin $newtag
```

### Deleting tag

```
# delete local tag, then remote
todel=v1.0.0
git tag -d $todel && git push --delete origin $todel
