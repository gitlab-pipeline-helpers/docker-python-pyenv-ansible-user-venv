#
# Multi-stage build for latest 'git' binary and system certs
# Then pyenv installed in main image for latest Python
#

#=======================================================================
FROM debian:bookworm-20240612-slim as cert-builder

ENV DEBIAN_FRONTEND=noninteractive

# latest system certs
RUN \
  echo "starting cert update" \
  && apt-get update \
  && apt-get install -y --no-install-recommends -o Dpkg::Options::="--force-confnew" software-properties-common wget \
  && update-ca-certificates \
  && echo "cert update done"


#=======================================================================
FROM debian:bookworm-20240612-slim as git-builder

ENV DEBIAN_FRONTEND=noninteractive

ENV GIT_VERSION=2.45.2

# install git
RUN \
  echo "starting git build" \
  && apt-get update \
  && apt-get install -y --no-install-recommends -o Dpkg::Options::="--force-confnew" wget make unzip dh-autoreconf libcurl4-openssl-dev libexpat1-dev gettext libz-dev libssl-dev install-info \
  && wget https://github.com/git/git/archive/refs/tags/v${GIT_VERSION}.tar.gz -O git.tar.gz --no-check-certificate \
  && tar xfz git.tar.gz \
  && cd git-${GIT_VERSION} \
  && make configure && ./configure --prefix=/usr/local && make -j2 all && make -j2 install \
  && which git \
  && git --version \
  && echo "git compile looks good"


#=======================================================================
FROM debian:bookworm-20240612-slim

# copies git single binary and support directory
COPY --from=git-builder /usr/local/bin/git /usr/local/bin/.
COPY --from=git-builder /usr/local/libexec/git-core /usr/local/libexec/git-core

# copies entire directory of updated certs and linked certs
COPY --from=cert-builder /etc/ssl/certs /etc/ssl/certs
COPY --from=cert-builder /usr/share/ca-certificates/mozilla /usr/share/ca-certificates/mozilla

RUN \
  echo "basic apt update and install" \
  && apt-get update \
  && apt-get install -q -y --no-install-recommends -o Dpkg::Options::="--force-confnew" wget curl vim whois dos2unix jq \
  && echo "done"

# install libcurl4-openssl-dev needed for git
# install dependencies for pyenv
# https://gist.github.com/jprjr/7667947
RUN \
  echo "installing dependencies for pyenv" \
  && apt-get install -y --no-install-recommends libcurl4-openssl-dev \
  && apt-get install -q -y --no-install-recommends -o Dpkg::Options::="--force-confnew" make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python3-openssl mecab-ipadic-utf8

RUN \
  echo "cleanup apt" \
  && rm -fr /var/lib/apt/lists/*

# non-root for security
RUN \
  addgroup pygroup --gid 9000 \
  && adduser pyuser --uid 9000 --shell /bin/bash --gid 9000
SHELL ["/bin/bash", "-c"]
USER pyuser

WORKDIR $HOME
RUN \
  echo "going to set permissions for HOME at $HOME" \
  && echo "pwd =" $(pwd) \
  && echo "whoami =" $(whoami) \
  && chmod g+rwx $HOME \
  && chmod g+s $HOME \
  && echo "done"

ENV PYTHON_VERSION=3.11.9
ENV PYENV_ROOT="/home/pyuser/.pyenv"
ENV PATH="$PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH"

# Install pyenv then requested Python version
# https://gist.github.com/jprjr/7667947
RUN set -ex \
    && curl https://pyenv.run | bash \
    && which pyenv \
    && pyenv update \
    && pyenv install $PYTHON_VERSION \
    && pyenv global $PYTHON_VERSION \
    && pyenv rehash \
    && echo "done installing pyenv"

RUN \
  echo "show effective Python version" \
  && which python \
  && which python3 \
  && which pip \
  && which pip3 \
  && python --version \
  && python3 --version \
  && pip --version \
  && pip3 --version \
  && pip3 install --upgrade pip

# install pip modules and Ansible galaxy modules
# Markupsafe pinned to avoid module missing error, paramiko included to avoid blowfish error
COPY --chown=pyuser requirements.txt ./
# needed by ansible-galaxy to find certs
ENV SSL_CERT_DIR=/etc/ssl/certs
RUN \
  echo "starting pip and ansible-galaxy module installs" \
  && pip3 list \
  && pip3 install --upgrade setuptools \
  && pip3 install 'resolvelib<0.6.0' 'Markupsafe==1.1.1' requests paramiko \
  && pip3 list \
  && pip3 install -r requirements.txt \
  && pip3 install ansible-core \
  && ansible-galaxy collection install community.general:7.5.0 ansible.posix:1.5.4 community.kubernetes:2.0.1 community.crypto:2.15.1 ansible.netcommon:5.2.0 \
  && pip3 list \
  && pip3 freeze \
  && ansible --version

WORKDIR /home/pyenv

# if debugging
ENTRYPOINT [ "/bin/bash" ]

